# slfvcoaltimes

## Description: 

This project contains a python implementation of the "smooth" approximation proposed in Wirtz & Guindon, 2022 of the coalescence probability P(t) of lineage pairs under the spatial Lambda-Fleming-Viot process (Etheridge et al. 2013). 

## Requirements

The scripts are written in python and require the packages numpy and math. It is recommended to use pyhon 3.5 or higher. The main method is contained in "approx-distribution.py" and makes use of "gprimesthd0.py", the latter thus needs to be in the same folder for "approx-distribution.py" to run as is.

## Usage

The usual command is

```
python3 approx-distribution.py n d0 theta2 x
```

where n is the degree of the Taylor polynomial, d0 is the initial distance, theta2 the dispersal variance, i.e. theta^2, where theta is the respective parameter from the original paper. The value x is the breakoff point; for t between 0 and x, the Taylor polynomial will be used to approximate P(t), for higher values the Gamma approximation with smooth transition. It should be chosen such that the Taylor approximation at x is still reasonably good; a math error may occur when x is too large.
The output is an array of values of approximations P(t), with t ranging from 0 to 10, in increments of 0.01.

## Contributing

I am open to any contributions that aim to make the program more flexible, render the output more suitable for statistical purposes or to provide it in a compiled version.

## License

Licensed under CC BY-NC.
