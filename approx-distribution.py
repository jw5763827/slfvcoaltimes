#A module combining Taylor series and Gamma approximation scheme for the distribution of T_0. Lambda assumed to be 1. Builds on gprimesthd0.py.

import gprimesthd0
import numpy as np
import math
import sys

n = int(sys.argv[1])				#Degree of Taylor polynomials
d0 = float(sys.argv[2])			#Initial distance between lineages
theta2 = float(sys.argv[3])			#Dispersal variance
x = float(sys.argv[4])				#Break-point between Taylor and Gamma approximation

pcoeffs = []					#Taylor coeffs of the probability distribution
ecoeffs = []					#Coeffs of expected Z_t

def arrays(n,d0,theta2):			#calculates the coefficients
  for i in range(n):
    pcoeffs.append(gprimesthd0.gprime(i,0.0,d0,theta2))
    ecoeffs.append(gprimesthd0.gprimes(i,0.0,d0,theta2))

def tseries(coeffs,x):				#returns value of a polynomial with coefficients specified
  z=0.0
  for i in range(len(coeffs)):
     z=z+coeffs[i]*x**i/math.factorial(i)
  return z

def dtseries(coeffs,x):			#returns value of the derivative of a polynomial with coefficients specified
  z=0.0
  for i in range(len(coeffs)-1):
     z=z+coeffs[i+1]*x**i/math.factorial(i)
  return z     

#debyp and varadjusted are necessary calculations to guarantee the smooth transition from Taylor series to Gamma approximation of the probability. See Eq. 59 in Rate of coalescence of lineage pairs in the Spatial Lambda-Fleming–Viot process

def debyp(x):					#calculates the differential of the expected distance conditioned on non-coalescence
  z=(tseries(ecoeffs,x)/tseries(pcoeffs,x)-tseries(ecoeffs,x-0.0001)/tseries(pcoeffs,x-0.0001))/0.0001
  return z       
  
def varadjusted(x,theta2):			#calculates the value of the variance of Z_t that guarantees a smooth transition at x. May produce 							 math error, e.g. if x is out of "good" range for the taylor polynomial.
  y=-math.log(-2*dtseries(pcoeffs,x)/tseries(pcoeffs,x)/(2*math.pi*theta2))*tseries(pcoeffs,x)**2/tseries(ecoeffs,x)**2
  v=0
  i=-1
  while(round(v)==0):				#A Newton routine to approximate the requested variance
    i=i+1
    v=tseries(ecoeffs,x)**2+i
#    print(v)
    vnew=v+((1-tseries(pcoeffs,x)/tseries(ecoeffs,x)*v)-math.exp(y)**v)/(tseries(pcoeffs,x)/tseries(ecoeffs,x)+y*math.exp(y)**v)
#    print(vnew)
    while(vnew-v>0.0001 or v-vnew>0.0001):
      v=vnew
      vnew=v+((1-tseries(pcoeffs,x)/tseries(ecoeffs,x)*v)-math.exp(y)**v)/(tseries(pcoeffs,x)/tseries(ecoeffs,x)+y*math.exp(y)**v)
#      print(vnew)
  return vnew
  
  
def papprox(y,x,varadjusted,theta2):		#returns the value of the distribution at x, varadjusted is the (precomputed) smooth-transition 							 variance
  if(y < x):
    return tseries(pcoeffs,y)
  else:
    j=0.01
    ynew=tseries(pcoeffs,x)
    while(j < y-x):
      exponent=-(-tseries(ecoeffs,x)/tseries(pcoeffs,x)-debyp(x)*(j))**2/(varadjusted*(x+j)**2/(x**2))
      base=(1+varadjusted*(x+j)**2/(x**2)/(-tseries(ecoeffs,x)/tseries(pcoeffs,x)-debyp(x)*(j)))
      ynew2 = ynew * math.exp(-math.pi*theta2*(base**exponent)*0.01)
#      print(ynew2<=ynew)
      j=j+0.01
      ynew=ynew2
    return ynew
    
arrays(n,d0,theta2)				#generate the arrays
values = []					#initialize return array 
var = varadjusted(x,theta2)			#calculate smooth-transition variance
for i in range(101):
  values.append(1-papprox(i/10,x,var,theta2))	#calculate sequence of values of the distribution
  
  
print(*values, sep=' ')					#return
