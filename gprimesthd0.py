#This module provides the recursive functions necessary for calculating coefficients of $M^*_{D_t}(s)$ with $theta2$ and $d_0$.
#gprime is for the probability, gprimes for first and gprimess for second moment.

import math

def gprime(k,s,d0,theta2):
 if k==0:
  return math.exp(-s*d0/(4*theta2));
 else:
  x=4*math.pi*theta2*(-gprime(k-1,s,d0,theta2)+float(1)/float(4)*gprime(k-1,1+s,d0,theta2)+1/(s+1)*gprime(k-1,s/(s+1),d0,theta2)-2/(3*s+4)*gprime(k-1,1+s/(3*s+4),d0,theta2));
  return x;


def gprimes(k,s,d0,theta2):
 if k==0:
  return -d0/(4*theta2) * math.exp(-s*d0/(4*theta2));
 else:
  x=4*math.pi*theta2*(-gprimes(k-1,s,d0,theta2)+float(1)/float(4)*gprimes(k-1,1+s,d0,theta2)-(1/(s+1)**2)*gprime(k-1,s/(s+1),d0,theta2)+1/((s+1)**3)*gprimes(k-1,s/(s+1),d0,theta2)+6/((3*s+4)**2)*gprime(k-1,1+s/(4+3*s),d0,theta2)-32/((3*s+4)**3)*gprimes(k-1,1+s/(3*s+4),d0,theta2));
  return x;  

